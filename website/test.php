<?php
// Include classes files
require 'Host.php';
require 'LoadBalancer.php';
require 'Request.php';

// Create hosts
$host1 = new Host('Server 1');
$host2 = new Host('Server 2');
$host3 = new Host('Server 3');
$host4 = new Host('Server 4');
$host5 = new Host('Server 5');

// Init load balancer with sequential test
$loadBalancer = new LoadBalancer([$host1, $host2, $host3, $host4,  $host5], "loadAverage");
//$loadBalancer = new LoadBalancer([$host1, $host2, $host3, $host4,  $host5], "sequential");

// Handle Request
$loadBalancer->handleRequest(new Request());
$loadBalancer->handleRequest(new Request());
$loadBalancer->handleRequest(new Request());
$loadBalancer->handleRequest(new Request());
$loadBalancer->handleRequest(new Request());
$loadBalancer->handleRequest(new Request());
$loadBalancer->handleRequest(new Request());
$loadBalancer->handleRequest(new Request());