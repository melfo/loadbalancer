<?php

/**
 * LoadBalancer class
 *
 * @author gthirard<thirardg@gmail.com>
 */
class LoadBalancer
{

    /**
     * Maximum charge authorized by host
     */
    CONST MAX_LOAD_CHARGE = 0.75;

    /**
     * @var Host[] - List of available hosts
     */
    private $hosts;

    /**
     * @var String - Algorithm to use to balance
     */
    private $algorithm;

    /**
     * @var integer - Last index used in Hosts list
     */
    private $currentHostIndex;

    /**
     * LoadBalancer constructor.
     *
     * @param Host[] $hosts - List of hosts instances
     * @param $algorithm - Algorithm that used to balance between hosts
     */
    public function __construct($hosts, $algorithm)
    {
        // Control if algorithm is available to use
        if (!in_array($algorithm, $this->getAvailableAlgorihtms())) {
            throw new Exception('Algorithm unknown');
        }

        // Assign class variables
        $this->algorithm = $algorithm;
        $this->hosts = $hosts;
        $this->currentHostIndex = NULL;
    }

    /**
     * Handle the request between all host
     *
     * @param Request $request
     * @throws Exception
     */
    public function handleRequest(Request $request)
    {
        // Use the right method to get the host who will treat the request
        if ($this->algorithm == "sequential") {
            $host = $this->getNextHost();
        } else if ($this->algorithm == "loadAverage") {
            $host = $this->getFirstAvailableHost();
        }

        // Redirect the request to host handleRequest function
        $host->handleRequest($request);
    }

    /**
     * List all algorithms managed with this load balancer instance
     *
     * @return array
     */
    private function getAvailableAlgorihtms()
    {
        return [
            'sequential',
            'loadAverage'
        ];
    }

    /**
     * Return the first host under max load average or the host with the lowest load
     *
     * @return Host
     */
    private function getFirstAvailableHost()
    {
        $availableHost = $hostWithLowestLoad = NULL;

        foreach ($this->hosts as $host) {
            if ($host->getLoad() < self::MAX_LOAD_CHARGE) {
                // If the host has a load under max load we can return this
                $availableHost = $host;
                break;
            } else if ($hostWithLowestLoad === NULL || $host->getLoad() < $hostWithLowestLoad->getLoad()) {
                // If current host has load over the maximum allowed but his current load is under all others host
                $hostWithLowestLoad = $host;
            }
        }

        return ($availableHost !== NULL) ? $availableHost : $hostWithLowestLoad;
    }

    /**
     * Return the next host in rotation
     *
     * @return Host
     */
    private function getNextHost()
    {
        // Optimize time to return if the current host isn't initialized with Host instance, the first Host in list can directly be returned
        if ($this->currentHostIndex === NULL) {
            $this->currentHostIndex = 0;
            return $this->hosts[0];
        }

        $this->currentHostIndex++;

        if (isset($this->hosts[$this->currentHostIndex])) {
            // If current host has a next host so we return the next host
            return $this->hosts[$this->currentHostIndex];
        } else {
            // If current host doesn't has a next so return the first host
            $this->currentHostIndex = 0;
            return $this->hosts[0];
        }
    }
}