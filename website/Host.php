<?php

/**
 * Host class
 *
 * @author gthirard<thirardg@gmail.com>
 */
class Host
{

    private $name;
    private $load;

    public function __construct($name)
    {
        $this->name = $name;

        // Generate random load between 0 and 1
        $this->load = rand(0,100) / 100;

        echo "Server $this->name : Initinialized with load : " . $this->load . '<br>';
    }

    public function handleRequest(Request $request)
    {
        // Echo the current server
        echo $this->name . ' Load : ' .$this->load;

        // Add charge to simulate the charge of the treatment
        $this->load += 0.3;

        echo ' New charge : ' . $this->load . '<br>';
    }

    public function getLoad()
    {
        return $this->load;
    }
}