# Global description

PHP Load balancer
Load balancer implement 2 differents algorithms:
* sequential: pass the request sequentially in rotation to each hosts
* loadAverage: pass the request to the first host that has a load under 0.75

All PHP code are on website/ folder.

# Configuration
Use `vagrant up` in this folder to provide a webserver.

You need to add the following line on your hosts file `127.0.0.1 dev.loadbalancer.fr`

After this 2 steps, you can access to test.php with the [following url](http://dev.loadbalancer.fr:8080/test.php)

# Author
Guillaume Thirard - thirardg@gmail.com